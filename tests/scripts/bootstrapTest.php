<?php

namespace Davek1312\Config\Tests\Scripts;

class BootstrapTest extends \PHPUnit_Framework_TestCase {

    public function testDotenvBootstrapped() {
        $this->assertEquals('TEST', getenv('TEST'));
    }

    public function testEnv() {
        $this->assertEquals('TEST', davek1312_env('TEST'));
        $default = 'DEFAULT';
        $this->assertEquals($default, davek1312_env('NON_EXISTENT', $default));
    }
}