<?php

namespace Davek1312\Config\Tests;

use Davek1312\Config\Config;

class ConfigTest extends \PHPUnit_Framework_TestCase {

    public function test__construct() {
        $config = new Config();
        $testItems = [
            'test' => [
                'key' => 'value',
            ]
        ];
        $this->assertConfigurationFilesLoaded($config, $testItems);

        $defaultItems = [
            'key' => 'value',
        ];
        $config = new Config($defaultItems);
        $this->assertConfigurationFilesLoaded($config, $defaultItems);
    }

    private function assertConfigurationFilesLoaded(Config $config, array $items) {
        $this->assertEquals($items, $config->all());
    }
}