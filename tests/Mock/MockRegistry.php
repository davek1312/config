<?php

namespace Davek1312\Config\Tests\Mock;

class MockRegistry extends \Davek1312\App\Registry {

    protected function register() {
        $this->registerConfig('tests/Mock/config/');
    }
}