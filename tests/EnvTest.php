<?php

namespace Davek1312\Config\Tests;

use Davek1312\Config\Env;

class EnvTest extends \PHPUnit_Framework_TestCase {

    public function testGetEnv() {
        $this->assertEquals('TEST', Env::getEnv('TEST'));
        $this->assertNull(Env::getEnv('NON_EXISTENT'));
        $default = 'DEFAULT';
        $this->assertEquals($default, Env::getEnv('NON_EXISTENT', $default));
    }
}