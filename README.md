# davek1312/config

Bootstraps vlucas/phpdotenv and illuminate/config packages.

# Installation

The package is available on [Packagist](https://packagist.org/packages/davek1312/config),
you can install it using [Composer](https://getcomposer.org/).

```bash
composer require davek1312/config
```

# Configuration

## Environment Variables
Copy `vendor/davek1312/config/davek1312` folder to your application's root directory and rename `config/.env.example` to `.env`. View the `.env` data format on [GitHub](https://github.com/vlucas/phpdotenv/blob/master/README.md).
You should not commit your `.env` file to your version control system.

## Register Configuration Files
To register your configuration files view the [davek1312\app](https://packagist.org/packages/davek1312/app) documentation.

Add your configuration files to the registered configuration folder.
```php
//configuration folder/test.php
<?php 
return [
    'key' => 'value',
];
```

# Usage

## Environment Variables
You can use any of the methods from the [vlucas/phpdotenv](https://github.com/vlucas/phpdotenv) package.
You can also use the `davek1312_env` function.
```php
// If $key is not found in your .env file $default will be returned
davek1312_env($key, $default = null);
```

## Configuration Files

Retrieving configuration values:
```php
use Davek1312\Config\Config;

$config = new Config();
$value = $config->get('tests.key'); //Will return 'value';
$value = $config->get('tests.non_existent'); //Will return NULL;
```

All other methods for the `Config` class can be found [here](https://laravel.com/api/5.4/Illuminate/Config/Repository.html).