<?php

namespace Davek1312\Config;

use Davek1312\App\App;
use Illuminate\Config\Repository;
use Symfony\Component\Finder\Finder;

/**
 * Wrapper for Illuminate\Config\Repository
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class Config extends Repository {

    /**
     * Create a new configuration repository.
     *
     * @param array  $items
     */
    public function __construct(array $items = []) {
        if(empty($items)) {
            $this->loadConfigurationFiles();
        }
        else {
            parent::__construct($items);
        }
    }

    /**
     * Load the configuration items from all of the files.
     */
    private function loadConfigurationFiles() {
        foreach($this->getConfigurationAllFiles() as $fileKey => $path) {
            $this->set($fileKey, require $path);
        }

        $files = $this->getConfigurationAllFiles();
        foreach($files as $fileKey => $path) {
            $envConfig = require $path;

            foreach ($envConfig as $envKey => $value) {
                $this->set($fileKey.'.'.$envKey, $value);
            }
        }
    }

    /**
     * Get all configuration files
     *
     * @return array
     */
    private function getConfigurationAllFiles() {
        $files = [];
        $paths = $this->getConfigFilePaths();

        foreach($paths as $path) {
            $files = array_merge($files, $this->getConfigurationFiles($path));
        }

        return $files;
    }

    /**
     * Get the configuration files for a path
     *
     * @param string $path
     *
     * @return array
     */
    private function getConfigurationFiles($path) {
        $files = [];
        if(!is_dir($path)) {
            return [];
        }

        $phpFiles = Finder::create()->files()->name('*.php')->in($path)->depth(0);
        foreach($phpFiles as $file) {
            $realPath = $file->getRealPath();
            $files[basename($realPath, '.php')] = $realPath;
        }

        return $files;
    }

    /**
     * Return an array of directories of the config files
     *
     * @return array
     */
    private function getConfigFilePaths() {
        $app = new App();
        return $app->getConfig();
    }
}