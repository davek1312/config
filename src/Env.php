<?php

namespace Davek1312\Config;

/**
 * Helpful environment variables methods
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class Env {

    /**
     * Returns the value for the provided key located in the .env file. If the value does not exists it will return $default.
     *
     * @param string $key
     * @param string $default
     *
     * @return string
     */
    public static function getEnv($key, $default = null) {
        $value = getenv($key);
        return $value === false ? $default : $value;
    }
}