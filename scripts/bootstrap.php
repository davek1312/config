<?php

try {
    $app = new \Davek1312\App\App();
    $dotenv = new Dotenv\Dotenv("{$app->getRootPackageDir()}/davek1312/config");
    $dotenv->load();
}
catch(Dotenv\Exception\InvalidPathException $e) {
}

if(!function_exists("davek1312_env")) {
    function davek1312_env($key, $default = null) {
        return \Davek1312\Config\Env::getEnv($key, $default);
    }
}